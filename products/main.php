<?php
/*
Plugin Name: Produkte-Verwaltung (Posts-Erweiterung)
Plugin URI: https://tobe.defin.ed/plugins/products
Description: Angepasster Post-Type mit Artikel Nr. und Kategorieverwaltung
Version: 1.0
Author: Dominik Ponniah
Author URI: https://dominik.ponniah.ch
*/

// The following methods are being used in this custom post type.
// Executed on initialization
add_action("init", "create_custom_products_taxonomy");
add_action("init", "products_post_type");

// Executed when adding metaboxes
add_action("add_meta_boxes", "add_details_metabox");

// Executed when saving the post
add_action("save_post", "product_details_data");

// Executed when adding a shortcut
add_shortcode("products-list", "create_product_list_shortcut");

/**
 * Creating a new Taxonomy called 'Produktkategorien'
 *
 */

function create_custom_products_taxonomy()
{
    // Labels for the user interface to be easier to understand.
    $labels = [
        "name" => __("Produktkategorien"),
        "singular_name" => __("Produktkategorie"),
        "search_items" => __("Kategorie suchen"),
        "all_items" => __("Alle Produktkategorien"),
        "parent_item" => __("Übergeordnete Kartegorie"),
        "parent_item_colon" => __("Übergeordnete Kategorie:"),
        "edit_item" => __("Kategorie bearbeiten"),
        "update_item" => __("Kategorie aktualisieren"),
        "add_new_item" => __("Neue Produktkategorie hinzufügen"),
        "new_item_name" => __("Name der Produktkategorie"),
        "menu_name" => __("Produktkategorien"),
    ];

    // Registering a new taxoonomy
    register_taxonomy(
        "product_categories",
        ["product_categories"],
        [
            "hierarchical" => true,
            "labels" => $labels,
            "show_ui" => true,
            "show_in_rest" => true,
            "show_admin_column" => true,
            "query_var" => true,
            "rewrite" => [
                "slug" => "product_categories",
            ],
        ]
    );
}

/**
 * Creating a new 'post'-Type named 'Produkte'
 *
 */
function products_post_type()
{
    // Labels for the user interface to be easier to understand.
    $labels = [
        "name" => __("Produkte"),
        "singular_name" => __("Produkt"),
        "add_new" => __("Hinzufügen"),
        "add_new_item" => __("Neues Produkt hinzufügen"),
        "edit_item" => __("Produkt bearbeiten"),
        "new_item" => __("Neues Produkt hinzufügen"),
        "view_item" => __("Produkt anzeigen"),
        "search_items" => __("Produkt suchen"),
        "not_found" => __("Keine Produkte gefunden"),
        "not_found_in_trash" => __(
            "Es befinden sich keine Produkte im Papierkorb"
        ),
    ];

    // Registering a new post type
    register_post_type("products", [
        "labels" => $labels,
        "public" => true,
        "capability_type" => "post",
        "rewrite" => [
            "slug" => "products",
        ],
        "has_archive" => true,
        "menu_position" => 2,
        "menu_icon" => "dashicons-products",
        "taxonomies" => ["product_categories"],
        "supports" => ["title", "editor", "thumbnail"],
    ]);
}

/**
 * Adding the metabox with its content (currently 'article number'
 * as string-input) to the Products-Post-Page.
 *
 */
function add_details_metabox()
{
    add_meta_box(
        "article_number",
        "Produktdetails",
        "generate_custom_metabox",
        "products",
        "side",
        "high"
    );
}

/**
 * Generates a readable HTML-Snippet containing the article number
 *
 * @param WP_Post $post The current page
 */
function generate_custom_metabox($post)
{
    wp_nonce_field("article_number", "article_number_nonce");
    $value = get_post_meta($post->ID, "_article_number", true);
    echo '<label for="article_number">Artikel Nr.</label> ';
    echo '<input type="text" id="article_number" name="article_number" value="' .
        esc_attr($value) .
        '" size="25" />';
}

/**
 * Stores the data from the form inside the database.
 *
 * @param int $post_id The id of the current post.
 */
function product_details_data($post_id)
{
    // Security checks before running the storing-procedure.
    if (!isset($_POST["article_number_nonce"])) {
        return;
    }
    if (!wp_verify_nonce($_POST["article_number_nonce"], "article_number")) {
        return;
    }
    if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
        return;
    }
    if (isset($_POST["post_type"]) && "page" == $_POST["post_type"]) {
        if (!current_user_can("edit_page", $post_id)) {
            return;
        }
    } else {
        // Check if user can edit this post (permissions)
        if (!current_user_can("edit_post", $post_id)) {
            return;
        }
    }
    if (!isset($_POST["article_number"])) {
        return;
    }
    // Sanitizing the form's content
    $my_data = sanitize_text_field($_POST["article_number"]);

    // Updating the data as _article_number
    update_post_meta($post_id, "_article_number", $my_data);
}

/**
 * Generates a shortcut [product-list] for all products.
 * Used IKEA-Design as inspiration.
 *
 */
function create_product_list_shortcut()
{
    // Defining to use only posts from type 'products',
    // all posts available (-1) and only those which are published already.
    $args = [
        "post_type" => "products",
        "posts_per_page" => -1,
        "publish_status" => "published",
    ];

    // Getting the Posts
    $query = new WP_Query($args);

    // If there are posts; iterating through them.
    if ($query->have_posts()):
        while ($query->have_posts()):
            $query->the_post();

            // Generating a simple UI with Thumbnail and Article Number including Description and Title.
            $result .= '<table style="border: none; width:80%;">';
            $result .= "<tbody>";
            $result .= "<tr>";
            $result .= '<td style="width: 150px;">';
            $result .=
                '<div class="image" style="background-image: url(' .
                get_the_post_thumbnail_url() .
                '); background-size: cover; height: 150px; width: 150px; background-position: center;">&nbsp;</div>';
            $result .= "</td>";
            $result .= '<td style="width: 100%; padding-left: 25px;">';
            $result .=
                '<p><span style="background-color: #333333; color: #ffffff; font-size: 17px;"><strong>' .
                get_post_meta(get_the_ID(), "_article_number", true) .
                "</strong></span></p>";
            $result .= "<p><strong>" . get_the_title() . "</strong></p>";
            $result .=
                '<p style="font-size: 12px; line-height: 1.15">' .
                get_the_content() .
                "</p>";
            $result .=
                '<a style="font-size: 13px;" href="' .
                get_the_permalink() .
                '">Zum Produkt</a>';
            $result .= "</td>";
            $result .= "</tr>";
            $result .= "</tbody>";
            $result .= "</table>";
        endwhile;

        wp_reset_postdata();
    endif;

    return $result;
}
